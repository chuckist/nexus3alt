#!/bin/sh
# $1 WEB_UID
# $2 WEB_PWD
# $3 WEB_URL


WEB_UID=$1
WEB_PWD=$2
WEB_URL=$3 #http://localhost/
WILDFLY_VERSION=23.0.2.Final
GEOSERVER_VERSION=2.17.5
GEOSERVER_PLUGIN_VERSION=2.17
OAUTH2_CORE_FILE=gs-sec-oauth2-core-2.17-20200512.101705-1.jar

# GeoServer
wget http://downloads.sourceforge.net/project/geoserver/GeoServer/${GEOSERVER_VERSION}/geoserver-${GEOSERVER_VERSION}-war.zip
curl -v -u ${WEB_UID}:${WEB_PWD} --upload-file geoserver-${GEOSERVER_VERSION}-war.zip ${WEB_URL}/geoserver-${GEOSERVER_VERSION}-war.zip
rm geoserver-${GEOSERVER_VERSION}-war.zip

# h2 plugin
wget https://master.dl.sourceforge.net/project/geoserver/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-h2-plugin.zip 
curl -v -u ${WEB_UID}:${WEB_PWD} --upload-file geoserver-${GEOSERVER_VERSION}-h2-plugin.zip ${WEB_URL}/geoserver-${GEOSERVER_VERSION}-h2-plugin.zip 
rm geoserver-${GEOSERVER_VERSION}-h2-plugin.zip

# imagemosaic-jdbc plugin
wget https://master.dl.sourceforge.net/project/geoserver/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-imagemosaic-jdbc-plugin.zip
curl -v -u ${WEB_UID}:${WEB_PWD} --upload-file geoserver-${GEOSERVER_VERSION}-imagemosaic-jdbc-plugin.zip ${WEB_URL}/geoserver-${GEOSERVER_VERSION}-imagemosaic-jdbc-plugin.zip
rm geoserver-${GEOSERVER_VERSION}-imagemosaic-jdbc-plugin.zip

# jdbcconfig plugin
wget https://build.geoserver.org/geoserver/${GEOSERVER_PLUGIN_VERSION}.x/community-latest/geoserver-${GEOSERVER_PLUGIN_VERSION}-SNAPSHOT-jdbcconfig-plugin.zip
curl -v -u ${WEB_UID}:${WEB_PWD} --upload-file geoserver-${GEOSERVER_PLUGIN_VERSION}-SNAPSHOT-jdbcconfig-plugin.zip ${WEB_URL}/geoserver-${GEOSERVER_PLUGIN_VERSION}-SNAPSHOT-jdbcconfig-plugin.zip
rm geoserver-${GEOSERVER_PLUGIN_VERSION}-SNAPSHOT-jdbcconfig-plugin.zip

# oracle plugin
wget https://master.dl.sourceforge.net/project/geoserver/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-oracle-plugin.zip 
curl -v -u ${WEB_UID}:${WEB_PWD} --upload-file geoserver-${GEOSERVER_VERSION}-oracle-plugin.zip ${WEB_URL}/geoserver-${GEOSERVER_VERSION}-oracle-plugin.zip 
rm geoserver-${GEOSERVER_VERSION}-oracle-plugin.zip

# python plugin
wget https://build.geoserver.org/geoserver/${GEOSERVER_PLUGIN_VERSION}.x/community-latest/geoserver-${GEOSERVER_PLUGIN_VERSION}-SNAPSHOT-python-plugin.zip 
curl -v -u ${WEB_UID}:${WEB_PWD} --upload-file geoserver-${GEOSERVER_PLUGIN_VERSION}-SNAPSHOT-python-plugin.zip ${WEB_URL}/geoserver-${GEOSERVER_PLUGIN_VERSION}-SNAPSHOT-python-plugin.zip 
rm geoserver-${GEOSERVER_PLUGIN_VERSION}-SNAPSHOT-python-plugin.zip

# wps plugin
wget https://master.dl.sourceforge.net/project/geoserver/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-wps-plugin.zip 
curl -v -u ${WEB_UID}:${WEB_PWD} --upload-file geoserver-${GEOSERVER_VERSION}-wps-plugin.zip ${WEB_URL}/geoserver-${GEOSERVER_VERSION}-wps-plugin.zip 
rm geoserver-${GEOSERVER_VERSION}-wps-plugin.zip

# authkey plugin
wget https://master.dl.sourceforge.net/project/geoserver/GeoServer/${GEOSERVER_VERSION}/extensions/geoserver-${GEOSERVER_VERSION}-authkey-plugin.zip 
curl -v -u ${WEB_UID}:${WEB_PWD} --upload-file geoserver-${GEOSERVER_VERSION}-authkey-plugin.zip ${WEB_URL}/geoserver-${GEOSERVER_VERSION}-authkey-plugin.zip 
rm geoserver-${GEOSERVER_VERSION}-authkey-plugin.zip

# oauth2 core library
wget https://maven.geo-solutions.it/org/geoserver/community/gs-sec-oauth2-core/${GEOSERVER_PLUGIN_VERSION}-SNAPSHOT/${OAUTH2_CORE_FILE}
curl -v -u ${WEB_UID}:${WEB_PWD} --upload-file ${OAUTH2_CORE_FILE} ${WEB_URL}/gs-sec-oauth2-core-${GEOSERVER_PLUGIN_VERSION}.jar
rm ${OAUTH2_CORE_FILE}

# oauth2 openid plugin
wget https://build.geoserver.org/geoserver/${GEOSERVER_PLUGIN_VERSION}.x/community-latest/geoserver-${GEOSERVER_PLUGIN_VERSION}-SNAPSHOT-sec-oauth2-openid-connect-plugin.zip
curl -v -u ${WEB_UID}:${WEB_PWD} --upload-file geoserver-${GEOSERVER_PLUGIN_VERSION}-SNAPSHOT-sec-oauth2-openid-connect-plugin.zip ${WEB_URL}/geoserver-${GEOSERVER_PLUGIN_VERSION}-SNAPSHOT-sec-oauth2-openid-connect-plugin.zip
rm geoserver-${GEOSERVER_PLUGIN_VERSION}-SNAPSHOT-sec-oauth2-openid-connect-plugin.zip

# Wildfly
wget https://download.jboss.org/wildfly/${WILDFLY_VERSION}/servlet/wildfly-servlet-${WILDFLY_VERSION}.tar.gz
curl -v -u ${WEB_UID}:${WEB_PWD} --upload-file wildfly-servlet-${WILDFLY_VERSION}.tar.gz ${WEB_URL}/wildfly-servlet-${WILDFLY_VERSION}.tar.gz
rm wildfly-servlet-${WILDFLY_VERSION}.tar.gz

# redis plugin
# NOTICE
# Download Redis release
# https://github.com/locationtech/geomesa/releases
# Compatibility table
# https://www.geomesa.org/documentation/stable/user/geoserver.html#geoserver-versions
# Extract and copy from dist/gs-plugins
# Plugin is named something like
# geomesa-redis-gs-plugin_2.11-3.2.0-install.tar.gz

# DoD CAs
#wget https://militarycac.com/maccerts/AllCerts.p7b
#zip AllCerts.zip AllCerts.p7b
#curl -v -u ${WEB_UID}:${WEB_PWD} --upload-file AllCerts.zip ${WEB_URL}/AllCerts.zip
#rm AllCerts.*


