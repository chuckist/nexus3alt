#!/bin/sh
# $1 WEB_UID
# $2 WEB_PWD
# $3 WEB_PORT

WEB_UID=$1
WEB_PWD=$2
WEB_PORT=$3

docker stop nexus3alt
docker rm nexus3alt

#docker run -v /home/duser/Projects/nexus3alt/dav:/var/lib/dav \
docker run -v /srv/dav:/var/lib/dav \
    -e AUTH_TYPE=Basic -e USERNAME=${WEB_UID} -e PASSWORD=${WEB_PWD} \
    --publish ${WEB_PORT}:80 -d --name nexus3alt bytemark/webdav

